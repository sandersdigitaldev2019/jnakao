var header = {
    'Accept': 'application/json',
    'REST-range': 'resources=0-10',
    'Content-Type': 'application/json; charset=utf-8'
};

var insertMasterData = function (ENT, loja, dados, fn) {
    $.ajax({
        url: '/api/dataentities/' + ENT + '/documents/',
        type: 'PATCH',
        data: dados,
        headers: header,
        success: function (res) {
            fn(res);
        },
        error: function (res) { }
    });
};

var selectMasterData = function (ENT, loja, params, fn) {
    $.ajax({
        url: '/api/dataentities/' + ENT + '/search?' + params,
        type: 'GET',
        headers: header,
        success: function (res) {
            fn(res);
        },
        error: function (res) { }
    });
};

$('.helperComplement').remove();

$mobile = 768;
$desconto = 6;
$price_avista = '';

function desconto(value1) {
    let best_price = value1;
    best_price = best_price / 100;
    best_price = best_price.toFixed(2);

    var initial = parseFloat((best_price * $desconto) / 100);
    var price = best_price - initial;
    price = price.toFixed(2);
    price = price.replace('.', ',');

    return price;
    console.log(price);
}

function getMoney(str) {
    var str = str.toString();
    return parseInt(str.replace(/[\D]+/g, ''));
}

function formatReal(int) {
    var tmp = int + '';
    tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
    if (tmp.length > 6)
        tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

    return tmp;
}

function flag_discount() {
    $('.flag_discount').each(function () {
        var descpct = $(this).text().replace(',', '.');
        descpct = descpct.replace(' %', '');
        descpct = parseFloat(descpct);
        descpct = descpct.toFixed(0);
        if ((descpct == "0") || (descpct == 0)) {
            $(this).hide();
        } else {
            $(this).show().addClass("active");
            $(this).html('<p>-' + descpct + '%</p>');
        }
    });
}
flag_discount();

function price_sold_out() {
    setTimeout(() => {
        $('.prateleira ul li article').each(function (i, el) {
            var id = $(el).data('id');

            if ($(el).find('.sold_out').length === 1) {
                $(el).addClass('disabled');
                $(el).parents('li').addClass('disabled');

                vtexjs.catalog.getProductWithVariations(id).done(function (product) {
                    $.ajax({
                        url: 'https://dev.sandersdigital.com.br/jnakao/consulta-estoque.php',
                        dataType: 'json',
                        method: 'POST',
                        action: 'POST',
                        data: {
                            productId: product.skus[0].sku
                        },
                        success: function (json) {
                            var json = JSON.parse(json);
                            if (!isNaN(json.costPrice)) {

                                let price = json.costPrice;
                                price = price.toString().replace('.', ',');

                                if ($(el).find('.product_price a .por').length === 0) {
                                    $(el).find('.product_price a').html('');
                                    $(el).find('.product_price a').html('<p class="por"><span>Por:</span><strong class="best_price">R$ ' + price + '</strong></p>');
                                    $(el).parents('li').addClass('active');
                                }
                            }
                        }
                    });
                });
            }
        });
    }, 1000);
}

function compre_junto() {
    if ($('.produto main .box_compre_junto .columns .column.column_1 .prateleira ul li').length) {

        $('.produto main .box_compre_junto').removeClass('dis-none');

        if (skuJson.skus.length === 1) {
            var buy = '/checkout/cart/add?sku=' + skuJson.skus[0].sku + '&qty=' + $('.product_info .box_count input[type="number"]').val() + '&seller=1&';
        } else {
            if (selectedToBuy[0] === undefined) {
                $('.box_compre_junto .column_2 article .buy').addClass('disabled');
            } else {
                $('.box_compre_junto .column_2 article .buy').removeClass('disabled');
                var buy = '/checkout/cart/add?sku=' + selectedToBuy[0] + '&qty=' + $('.product_info .box_count input[type="number"]').val() + '&seller=1&';
            }
        }
        var total = 0;
        $(skuJson.skus).each(function (a, b) {
            if (b.available === true) {
                total = total = b.bestPrice;
                $('.box_compre_junto .column_2 article p strong').text('R$ ' + formatReal(b.bestPrice));
            }
        });

        $('.box_compre_junto .prateleira ul li.adicionado article').each(function (a, b) {
            let data_id = $(b).data('id');

            vtexjs.catalog.getProductWithVariations(data_id).done(function (product) {
                total = total += product.skus[0].bestPrice;
                $('.box_compre_junto .column_2 article p strong').text('R$ ' + formatReal(total));

                buy = buy += 'sku=' + product.skus[0].sku + '&qty=1&seller=1&';
                $('.box_compre_junto .column_2 article .buy').attr('href', buy);
            });
        });
    }
}

var geral = (function () {
    var ambos = {
        search: function () {
            $('header .search input').on('input', function () {

                texto = $(this).val();

                if (texto != '') {
                    $('header .search .result').addClass('active');

                    $.ajax({
                        url: '/api/catalog_system/pub/products/search/' + texto,
                        type: 'GET',
                        dataType: 'json'
                    })
                        .done(function (success) {

                            var busca = '';

                            $(success).each(function (index, a) {
                                let categorias = a.categories[1].replace(/[0-9`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                                $('.digitando').html('<p>' + texto + '</p>' + '<p>' + texto + '<a href="' + categorias + '"> - ' + categorias + '</a></p>');

                                vtexjs.catalog.getProductWithVariations(a.productId).done(function (product) {
                                    if (product.available === true) {
                                        $(product.skus).each(function (index, b) {
                                            if (b.available === true) {
                                                console.log(b);
                                                var ID = a.productId;
                                                var nome = a.productName;
                                                var foto = a.items[0].images[0].imageUrl;
                                                var url = a.link;
                                                var de_price = b.listPriceFormated;
                                                var por_price = b.bestPrice;

                                                if (por_price != 0) {
                                                    busca += '<li data-id=' + ID + '>';
                                                    busca += '<a href="' + url + '">';
                                                    busca += '<div class="image"><img src="' + foto + '" /></div>';
                                                    busca += '<div class="name"><h3>' + nome + '</h3></div>';

                                                    if (de_price != por_price) {
                                                        busca += '<div class="box_price">';
                                                        busca += '<p class="por_price preco">à vista por:<span>R$ ' + formatReal(por_price) + '</span></p>';
                                                        busca += '</div>';
                                                    } else {
                                                        busca += '<div class="box_price">';
                                                        busca += '<p class="por_price preco">à vista por:<span>R$ ' + por_price + '</span></p>';
                                                        busca += '</div>';
                                                    }

                                                    busca += '</li>';

                                                    $('header .search .result ul').html(busca);
                                                }
                                            }
                                        });
                                    }
                                });
                            });
                        });
                } else {
                    $('header .search ul li, header .search .digitando p').remove();
                    $('header .search .result').removeClass('active');
                }

                $("header .search input").keypress(function (event) {
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if (keycode == '13') {
                        var texto = $(this).val();
                        location.href = '/' + texto;
                    }
                });

                $('header .row_1 .column.column_3 .search .input button').on('click', function () {
                    var texto = $('header .search input').val();
                    location.href = '/' + texto;
                });

                $('header .search .result').prepend('<span style="font-weight: bold;position: absolute;right: 15px;top: 15px;color: #3253a0;cursor:pointer;" class="close">X Fechar</span>');
                $('header .search .result .close').on('click', function () {
                    $('header .search ul li, header .search .digitando p, header .search .result .close').remove();
                    $('header .search .result').removeClass('active');
                });
            })
        },

        marcas: function () {
            $('.home .marcas ul').slick({
                autoplay: true,
                autoplaySpeed: 4000,
                infinite: true,
                arrows: true,
                dots: false,
                slidesToShow: 5,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        arrows: false
                    }
                }
                ]
            });
        },

        newsletter: function () {
            function isEmail(email) {
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                return regex.test(email);
            }
            var getEmail = $('.newsletter input[type="email"]').val();
            isEmail(getEmail)
            $('.newsletter input[type="submit"]').on('click', function () {
                var getEmail = $('.newsletter input[type="email"]').val();
                var dados = {
                    "email": getEmail
                }
                if (isEmail(getEmail)) {
                    $.ajax({
                        url: '/api/dataentities/NW/documents/',
                        type: 'PATCH',
                        data: JSON.stringify(dados),
                        headers: header,
                        success: function (res) {
                            swal('Obrigado!', 'Email Cadastrado com Sucesso.', 'success');
                            $('.newsletter input[type="email"]').val('')
                        },
                        error: function (res) { }
                    });
                } else {
                    swal('Opps!', 'Você precisa inserir um email valido', 'warning');
                }
            });
        }
    }

    var desktop = {
        todos_departamentos: function () {
            $('nav .column_1 ul li a')
                .mouseover(function () {
                    $('nav .column_2 .content').removeClass('active');
                    $('nav .column_2 .content.category_' + $(this).data('category')).addClass('active');

                    setTimeout(() => { }, 300);
                });

            $('nav .column_2 .content')
                .mouseover(function () {
                    $(this).addClass('active');
                })
                .mouseleave(function () {
                    $(this).removeClass('active');
                });
        }
    }

    var mobile = {
        tipbar: function () {
            $('.home .tipbar ul').slick({
                autoplay: true,
                autoplaySpeed: 4000,
                infinite: true,
                arrows: false,
                dots: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                ]
            });

            $('.home .tipbar').addClass('active');
        },

        hamburger: function () {
            $('header .menuMobile').on('click', function () {
                $('.menu .menu_1, #overlay, body').toggleClass('active');
            });

            $('.menu .menu_1 .close').on('click', function () {
                $('header .menuMobile').trigger('click');
            });
        },

        menuMobile: function () {
            $('.menu nav .column_1 a').on('click', function (e) {
                e.preventDefault();
                var getCat = $(this).attr('data-category');
                var getName = $(this).html();
                $('.menu nav').addClass('opened');
                $(".headMobile .name").html(getName)
                $(".headMobile .name").addClass('actived');
                $(".headMobile .back").addClass('actived');
                $(".menu nav .column_2").find(`.category_${getCat}`).parent().addClass('actived');
            });
            $('.menu .headMobile .back').on('click', function (e) {
                $('.menu nav').removeClass('opened');
                $(".headMobile .name").html('')
                $(".headMobile .name").removeClass('actived');
                $(".headMobile .back").removeClass('actived');
                $(".menu nav .column_2 li").removeClass('actived');
            })
        },


        conta: function () {
            $('.hover_mob').hide();
            $('.conta_mob').click(function (event) {
                $('.hover_mob').toggle(700);

            });
        },

        footerMobile: function () {
            $('.footer h2').on('click', function () {
                $(this).parent().toggleClass('opened')
            });
        }
    }

    ambos.search();
    ambos.marcas();
    ambos.newsletter();
    if ($('body').width() < $mobile) {
        mobile.hamburger();
        mobile.menuMobile();
        mobile.conta();
        mobile.tipbar();
        mobile.footerMobile();
    } else {
        desktop.todos_departamentos();
    }
})();

function open_carrinho() {
    $('header .minicart > a').trigger('click');
};

var carrinho = (function () {
    var geral = {
        toggle_carrinho: function () {
            $('#cart-lateral').removeClass('dis-none');

            $('header .minicart a').on('click', function (event) {
                event.preventDefault();

                desktop.cartLateral();

                $('#cart-lateral, body, #overlay').addClass('active');

                $('iframe[title="chat widget"]').css({
                    'opacity': '0',
                    'visibility': 'hidden'
                });
            });

            $('#cart-lateral .close').on("click", function (event) {
                event.preventDefault();
                $("#cart-lateral, body, #overlay").removeClass("active");

                $('iframe[title="chat widget"]').css({
                    'opacity': '1',
                    'visibility': 'visible'
                });
            });
        },
    };

    var desktop = {
        cartLateral: function () {
            vtexjs.checkout.getOrderForm().done(function (orderForm) {
                console.log(orderForm);

                $("#cart-lateral .columns").removeClass("loading");

                var quantidade = 0;

                for (var i = orderForm.items.length - 1; i >= 0; i--) {
                    quantidade = parseInt(quantidade) + parseInt(orderForm.items[i].quantity);
                }

                $("header .cart .qtd").text(quantidade);

                if (orderForm.value != 0) {
                    $("#cart-lateral .header .total-items span").text(orderForm.items.length + " Itens");
                    $("#cart-lateral .footer .total-price").text("R$ " + formatReal(orderForm.value));
                } else {
                    $('#cart-lateral .total-items span').text('0 Itens');
                    $('#cart-lateral .discount').html('Descontos: R$ 0,00');
                    $("#cart-lateral .footer .frete .value").html('<a href="/checkout" title="Calcular">Calcular</a>');
                    $("#cart-lateral .footer .total-price, #cart-lateral .footer .value-sub-total").text("R$ 0,00");
                }

                if (orderForm.totalizers.length != 0) {
                    $("#cart-lateral .footer .value-sub-total").text("R$ " + formatReal(orderForm.totalizers[0].value));

                    if (orderForm.totalizers.length === 3) {
                        $("#cart-lateral .discount").html("Descontos: R$ " + formatReal(orderForm.totalizers[1].value));
                        $("#cart-lateral .frete .value").html("R$ " + formatReal(orderForm.totalizers[2].value));
                    } else {
                        if (orderForm.shippingData.address != null) {
                            $("#cart-lateral .frete .value").html("R$ " + formatReal(orderForm.totalizers[1].value));
                        }
                    }
                }

                $("#cart-lateral .content ul li").remove();

                for (i = 0; i < orderForm.items.length; i++) {
                    var content = "";

                    content += '<li data-index="' + i + '">';
                    content += '<div class="column_1"><img src="' + orderForm.items[i].imageUrl + '" alt="' + orderForm.items[i].name + '"/></div>';
                    content += '<div class="column_2">';
                    content += '<div class="name">';
                    content += "<p>" + orderForm.items[i].name + "</p>";
                    content += "</div>";
                    content += '<div class="ft">';
                    content += "<ul>";
                    content += '<li data-index="' + i + '">';
                    if (orderForm.items[i].isGift === true) {
                        content += '<p class="gift"><strong>Brinde!</strong></p>';
                    } else {
                        content += '<div class="box-count">';
                        content += '<a href="" class="count count-down">-</a>';
                        content += '<input type="text" value="' + orderForm.items[i].quantity + '" />';
                        content += '<a href="" class="count count-up">+</a>';
                        content += "</div>";
                    }
                    content += "</li>";
                    content += '<li class="price">';
                    content += "<p class='de'>De: <span>R$ " + formatReal(orderForm.items[i].listPrice) + "</span></p>";
                    content += "<p class='por'><strong>Por: R$ " + formatReal(orderForm.items[i].sellingPrice) + "</strong></p>";
                    content += "</li>";
                    content += "</ul>";
                    content += "</div>";
                    content += "</div>";
                    content += '<span class="removeUni"><img src="/arquivos/ico-trash.png" alt="Remover Produto"/></span>';
                    content += "</li>";

                    $("#cart-lateral .content > ul").append(content);
                }
            });
        },

        changeQuantity: function () {
            $(document).on("click", "#cart-lateral .count", function (e) {
                e.preventDefault();

                $("#cart-lateral .columns").addClass("loading");

                var qtd = $(this).siblings('input[type="text"]').val();
                if ($(this).hasClass("count-up")) {
                    qtd++;
                    $(this).siblings('input[type="text"]').removeClass("active");
                    $(this).siblings('input[type="text"]').val(qtd);
                } else if ($(this).hasClass("count-down")) {
                    if ($(this).siblings('input[type="text"]').val() != 1) {
                        qtd--;
                        $(this).siblings('input[type="text"]').val(qtd);
                    } else {
                        $(this).siblings('input[type="text"]').addClass("active");
                    }
                }

                var data_index = $(this).parents("li").data("index");
                var data_quantity = $(this).parents("li").find('.box-count input[type="text"]').val();

                vtexjs.checkout.getOrderForm().then(function (orderForm) {
                    var total_produtos = parseInt(orderForm.items.length);
                    vtexjs.checkout
                        .getOrderForm()
                        .then(function (orderForm) {
                            var itemIndex = data_index;
                            var item = orderForm.items[itemIndex];

                            var updateItem = {
                                index: data_index,
                                quantity: data_quantity,
                            };

                            return vtexjs.checkout.updateItems(
                                [updateItem],
                                null,
                                false
                            );
                        })
                        .done(function (orderForm) {
                            desktop.cartLateral();
                        });
                });
            });
        },

        removeItems: function () {
            $(document).on("click", "#cart-lateral .removeUni", function () {
                $("#cart-lateral .columns").addClass("loading");

                var data_index = $(this).parents("li").data("index");
                var data_quantity = $(this).siblings("li").find('.box-count input[type="text"]').val();

                vtexjs.checkout
                    .getOrderForm()
                    .then(function (orderForm) {
                        var itemIndex = data_index;
                        var item = orderForm.items[itemIndex];
                        var itemsToRemove = [{
                            index: data_index,
                            quantity: data_quantity,
                        },];
                        return vtexjs.checkout.removeItems(itemsToRemove);
                    })
                    .done(function (orderForm) {
                        desktop.cartLateral();
                    });
            });
        },

        openCart: function () {
            $("#overlay").on("click", function () {
                if ($("#cart-lateral").hasClass("active")) {
                    $("#cart-lateral, #overlay").removeClass("active");
                }
            });
        },
    };

    geral.toggle_carrinho();

    desktop.cartLateral();
    desktop.changeQuantity();
    desktop.removeItems();
    desktop.openCart();
})();

var home = (function () {
    var geral = {
        logout: function () {
            $.ajax({
                url: '/api/vtexid/pub/authenticated/user',
                type: 'GET'
            }).done(function (user) {
                console.log('O cliente está logado.');

                if (user != null) {
                    $('header a.logout').removeClass('dis-none');
                    $('header .btn_account li.login').hide();

                    $.ajax({
                        url: '/api/dataentities/CL/search?_fields=firstName&email=' + user.user,
                        type: 'GET',
                        dataType: 'json'
                    })
                        .done(function (res) {
                            $('header p.client_name').html('Olá, ' + res[0].firstName + '!');
                        });
                }
            }).fail(function (user) {
                console.log('O cliente não está logado.');
            });
        },

        new_tag_blog: function () {
            $('a[href="https://blog.jnakao.com.br/"]').attr('target', '_blank');
        },

        new_tag_youtube: function () {
            $('a[href="https://www.youtube.com/channel/UCiDlngNuQo1YdQgfr5n2FYw"]').attr('target', '_blank');
        },

        prateleira: function () {
            $('.home .customshelf:not(.cards) .prateleira:not(.n3colunas) > ul, .pratileira_solucoes .prateleira > ul').slick({
                autoplay: false,
                autoplaySpeed: 4000,
                infinite: false,
                arrows: true,
                dots: false,
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        infinite: true,
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        variableWidth: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        infinite: true,
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        variableWidth: true
                    }
                }
                ]
            });
        },

        cards: function () {
            if ($('body').width() > $mobile) {
                if ($('.cards ul li').length > 4) {
                    $('.home .cards ul').slick({
                        autoplay: true,
                        autoplaySpeed: 4000,
                        infinite: false,
                        arrows: true,
                        dots: false,
                        slidesToShow: 4,
                        slidesToScroll: 1
                    });
                }
            } else {
                $('.home .cards ul').slick({
                    autoplay: true,
                    autoplaySpeed: 4000,
                    infinite: false,
                    arrows: true,
                    dots: false,
                    slidesToShow: 1,
                    slidesToScroll: 1
                });
            }
        },

        categorias: function () {
            $('.home .categories ul').slick({
                dots: false,
                infinite: false,
                speed: 300,
                slidesToShow: 5,
                slidesToScroll: 5,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: false
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    }
                ]
            });
        },

        mainBanner: function () {
            $('.mainbanner ul').slick({
                infinite: true,
                arrows: true,
                dots: false
            });
        }


    }

    var mobile = {
        banner_mobile: function () {
            $('.home .banner_mobile ul').slick({
                autoplay: true,
                autoplaySpeed: 4000,
                infinite: false,
                arrows: false,
                dots: true,
                slidesToShow: 1,
                slidesToScroll: 1
            });
        },

        three_banenrs: function () {
            $('.home .three_banners ul').slick({
                autoplay: true,
                autoplaySpeed: 4000,
                infinite: false,
                arrows: false,
                dots: false,
                slidesToShow: 1,
                slidesToScroll: 1
            });
        }
    }

    geral.logout();
    geral.new_tag_blog();
    geral.new_tag_youtube();
    geral.prateleira();
    // geral.cards();
    geral.mainBanner();
    geral.categorias();
    if ($('body').width() < $mobile) {
        mobile.banner_mobile();
        mobile.three_banenrs();
    }
})();

var produto = (function () {
    var desktop = {
        thumbs: function () {
            if ($('body').width() > $mobile) {
                if ($('.produto .thumbs li').length > 5) {
                    $('.produto .thumbs').removeClass('slick-initialized slick-slider slick-vertical');
                    $('.produto .thumbs').slick({
                        arrows: true,
                        vertical: true,
                        slidesToShow: 6,
                        slidesToScroll: 1,
                        verticalSwiping: true,
                        infinite: false
                    });
                }
            } else {
                if ($('.produto .thumbs li').length > 4) {
                    $('.produto .thumbs').removeClass('slick-initialized slick-slider slick-vertical');
                    $('.produto .thumbs').slick({
                        arrows: true,
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        infinite: false
                    });
                }
            }
        }
    }

    var geral = {
        video: function () {
            if ($('.value-field.Video iframe').length > 0) {
                $('.productDescription').append('<div class="video"></div>')
                $('.productDescription .video').append($('.value-field.Video iframe'));
                $('.productDescription .video iframe').fadeIn();
            }
        },

        reward_value: function () {
            if ($('#div_content_rewardValue .lblRewardValue').text() != '0,00') {
                $('#div_content_rewardValue').addClass('active');
            }
        },

        first_select: function () {
            $(window).load(function () {
                $('.produto main .product_info .box_2 .column_1 .sku-selector-container .topic li.skuList span label').removeClass('checked sku-picked');
                $('.produto main .product_info .box_2 .column_1 .sku-selector-container .topic li.skuList span input').prop('checked', false);

                if ($('.produto .sku-selector-container .topic li.skuList span label.item_unavailable').length || $('.produto .sku-selector-container .topic li.skuList span label').length > 1) {
                    $('.produto .sku-selector-container .topic li.skuList span label:not(.item_unavailable)').first().trigger('click');
                } else {
                    $('.produto .sku-selector-container .topic li.skuList span label').first().trigger('click');
                }
                if ($('.produto .sku-selector-container .topic li.skuList span label.item_unavailable').length == 2) {
                    $('.produto .sku-selector-container .topic li.skuList span label').first().trigger('click');
                }
            });
        },

        select_sku: function () {
            $(document).on('change', '.produto .sku-selector-container .topic li.skuList span input', function () {
                desktop.thumbs();
                compre_junto();

                if ($('.valor-dividido').length) {
                    $('.installments_custom').html($('.valor-dividido').html());
                }

                if ($('.skuBestPrice').length) {
                    $('.valor_total').html('<p>Valor total: ' + $('.skuBestPrice').text() + '</p>');
                }
            });

            if ($('.valor-dividido').length) {
                $('.installments_custom').html($('.valor-dividido').html());
            }

            if ($('.skuBestPrice').length) {
                $('.valor_total').html('<p>Valor total: ' + $('.skuBestPrice').text() + '</p>');
            }
        },

        prateleira: function () {
            $('.produto .prateleira:not(.n3colunas, .compre_junto) ul').slick({
                autoplay: false,
                autoplaySpeed: 4000,
                infinite: false,
                arrows: true,
                dots: false,
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        infinite: true,
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        variableWidth: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        infinite: true,
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        variableWidth: true
                    }
                }
                ]
            });
        },

        count: function () {
            $('.produto .count').on('click', function (e) {
                e.preventDefault();

                if ($(this).hasClass('count-mais')) {
                    var qtd_produtos = $('.box_count .content input').attr('value');
                    qtd_produtos++;
                } else if ($(this).hasClass('count-menos')) {
                    var qtd_produtos = $('.box_count .content input').attr('value');

                    if (qtd_produtos > 1) {
                        qtd_produtos--;
                    }
                }

                $('.box_count .content input').attr('value', qtd_produtos);

                let qty = parseInt(qtd_produtos);
                $('.buy-in-page-quantity').trigger('quantityChanged.vtex', [skuJson.productId, qty]);
                compre_junto();
            });
        },

        continue_buy: function () {
            window.alert = function () {
                open_carrinho();

                if (Cookies.get('IPS') != undefined) {
                    var IPS = Cookies.get('IPS');

                    console.log(IPS);

                    vtexjs.checkout.getOrderForm()
                        .then(function (orderForm) {
                            var marketingData = orderForm.marketingData;
                            marketingData = {
                                utmCampaign: (IPS.indexOf('Campanha') != -1 ? IPS.split('Campanha=')[1].split('&')[0] : ""),
                                utmiCampaign: (IPS.indexOf('Campanha') != -1 ? IPS.split('Campanha=')[1].split('&')[0] : ""),
                                utmMedium: (IPS.indexOf('Midia') != -1 ? IPS.split('Midia=')[1].split('&')[0] : ""),
                                utmSource: (IPS.indexOf('Parceiro') != -1 ? IPS.split('Parceiro=')[1].split('&')[0] : ""),
                            }

                            console.log(marketingData);

                            return vtexjs.checkout.sendAttachment('marketingData', marketingData)
                        }).done(function (orderForm) {
                            console.log('Promoção via UTM ativa.');
                        });
                }
            }
        },

        parcelamento: function () {
            $(document).on('click', '.produto .titulo-parcelamento', function () {
                $(document).find('.other-payment-method-ul').slideToggle();
            });
        },

        flag_discount_prod: function () {
            if ($('.product_price a').find('.retirar-na-loja').length) {
                $('.btn_buy').css('margin', '10px auto -20px');
            }

            if (selectedToBuy[0] === undefined) {
                if (skuJson.skus[0].discount != undefined) {
                    let init = skuJson.skus[0].discount / skuJson.skus[0].listPrice * 100;
                    init = init.toString().split('.')[0];
                    $('.valor-por ').append('<span class="flag_discount"><p>-<i>' + init + '</i><span>%</p></span>');
                }
            } else {
                let selected = skuJson.skus.filter(element => element.sku == selectedToBuy[0]);

                if (selected[0].discount != undefined) {
                    let init = selected[0].discount / selected[0].listPrice * 100;
                    init = init.toString().split('.')[0];
                    $('.valor-por ').append('<span class="flag_discount"><p>-<i>' + init + '</i><span>%</p></span>');
                }
            }
        },

        selosEntrega: function () {
            $.ajax({
                url: '/api/catalog_system/pub/products/search?fq=productId:' + skuJson.productId,
                type: 'GET'
            }).
                done(function (response, status) {

                    $('<div class="selos"></div>').insertAfter('.product_info .box_btn_buy');

                    if (response[0].productClusters[142]) { // FRETE GRÁTIS
                        $('.selos').append('<span class="frete-gratis"><img src="/arquivos/selo_frete_gratis.png" style="max-width: 45px; margin-right: 10px;"></span>');
                    }
                    if (response[0].productClusters[164]) { // JNAKAO EXPRESS
                        $('.selos').append('<span class="selo2h"><img src="/arquivos/flag-express.png"></span>');
                        $('<div id="selo2h"><span>Fechar</span><img src="/arquivos/pop-up-selo-express.jpg?v=2"></div>').insertAfter('.footer-marceneiro');
                    }
                    if (response[0].productClusters[168]) { // 24Horas
                        $('.selos').append('<span class="selo24h"><img src="/arquivos/flag-24h.png"></span>');
                        $('<div id="selo24h"><span>Fechar</span><img src="/arquivos/pop-up-selo-24horas.jpg?v=2"></div>').insertAfter('.footer-marceneiro');
                    }

                    if (response[0].productClusters[146]) { // RETIRAR NA LOJA
                        $('.selos').append('<span class="retirar-loja"><img src="/arquivos/flag_retire_na_loja.jpg" style="max-width: 125px; display: block;left: 50%;position: relative;transform: translateX(-50%);margin-top: 12px;"></span>');
                    }

                    $('.product_info .selos span').click(function (e) {
                        var seloId = $(this).attr('class');
                        $('#' + seloId).addClass('actived');
                        $('#overlay').addClass('popupSelo');
                    });

                    $('#selo2h span, #selo24h span').click(function (e) {
                        $(this).parent().removeClass('actived');
                        $('#overlay').removeClass('popupSelo');
                    })

                    $('#overlay').click(function (e) {
                        $('#selo2h, #selo24h ').removeClass('actived');
                        $('#overlay').removeClass('popupSelo');
                    })

                });

        }
    }

    if ($('body.produto').length) {
        geral.video();
        geral.reward_value();
        geral.first_select();
        geral.select_sku();
        geral.prateleira();
        geral.count();
        geral.continue_buy();
        geral.parcelamento();
        // geral.selosEntrega();
        desktop.thumbs();
        $(window).load(function () {
            compre_junto();
            geral.flag_discount_prod();
        });

        function productUni() {
            if ($('.produto main .product_info .box_2 .column_1 .sku-selector-container .topic li.skuList span label').length === 1 || $('.produto main .product_info .box_2 .column_1 .sku-selector-container .topic li.skuList span label').length === 0) {
                vtexjs.catalog.getCurrentProductWithVariations().done(function (product) {
                    if (skuJson.skus.length === 1) {
                        if (skuJson.skus[0].available === true) { } else {
                            console.log('sku unico - sem estoque');
                            vtexjs.catalog.getCurrentProductWithVariations().done(function (product) {
                                $.ajax({
                                    url: 'https://dev.sandersdigital.com.br/jnakao/consulta-estoque.php',
                                    dataType: 'json',
                                    method: 'POST',
                                    action: 'POST',
                                    data: {
                                        productId: skuJson.skus[0].sku
                                    },
                                    success: function (json) {
                                        var json = JSON.parse(json);
                                        if (!isNaN(json.costPrice)) {
                                            console.log(json);

                                            let list_price = json.listPrice;
                                            list_price = list_price.toString().replace('.', ',');

                                            let price = json.costPrice;
                                            price = price.toString().replace('.', ',');

                                            let content = '';
                                            content += '<p class="valor-de">DE: R$ ' + list_price + '</p>';
                                            content += '<p class="valor-por"><span>Por: </span><strong>R$ ' + price + '</strong></p>';
                                            $('.produto main .product_info .box_1 .column_1 .price_custom').html(content);
                                        }
                                    }
                                });
                            });
                        }
                    }
                });
            }
        }
        productUni();

        var selectedToBuy = new Array(0);
        var totalPriceBatchBuy = 0;
        var mainProductHasAggregateServices = 0;
        var seller;
        var actualPrice = 0;

        $(document).ready(function () {
            var batchBuyListener = new Vtex.JSEvents.Listener('batchBuyListener', BatchBuy_OnSkuDataReceived);
            skuEventDispatcher.addListener(skuDataReceivedEventName, batchBuyListener);
        });

        function BatchBuy_OnSkuDataReceived(e) {
            var id = e.skuData.id;
            selectedToBuy = [];
            if (id > 0) {
                sku_atual = e.skuData;

                $('.produto main .product_info .box_1 .column_1 .price_custom, .produto main .product_info .box_1 .column_2 .installments_custom, .produto main .product_info .box_1 .column_2 .valor_total').html('');

                if (sku_atual.availability != false) {
                    console.log('com estoque');
                } else {
                    console.log('sem estoque');

                    $.ajax({
                        url: 'https://dev.sandersdigital.com.br/jnakao/consulta-estoque.php',
                        dataType: 'json',
                        method: 'POST',
                        action: 'POST',
                        data: {
                            productId: sku_atual.id
                        },
                        success: function (json) {
                            var json = JSON.parse(json);
                            if (!isNaN(json.costPrice)) {
                                console.log(json);

                                let list_price = json.listPrice;
                                list_price = list_price.toString().replace('.', ',');

                                let price = json.costPrice;
                                price = price.toString().replace('.', ',');

                                let content = '';
                                content += '<p class="valor-de">DE: R$ ' + list_price + '</p>';
                                content += '<p class="valor-por"><span>Por: </span><strong>R$ ' + price + '</strong></p>';
                                $('.produto main .product_info .box_1 .column_1 .price_custom').html(content);
                            }
                        }
                    });
                }

                if (e.skuData.availability) {
                    $('.prateleiracompre.vitrinecompre').show();
                    selectedToBuy.add(id.toString());
                    totalPriceBatchBuy -= actualPrice;
                    totalPriceBatchBuy += e.skuData.price * 100;
                    actualPrice = e.skuData.price * 100;
                } else {
                    $('.prateleiracompre.vitrinecompre').hide();
                }
                CheckBatchProductsSelectedForBuying();
            }
        }

        $(document).ready(function () {
            if (!(typeof skuJson === 'undefined' || skuJson === null)) {
                CheckIfProdcutIsAvailable();
            }
            bindProductBatchBuyCheckboxes();
            bindBatchBuyButtons();
            if ($('.buy-product-checkbox-checked').length > 0) {
                var pId = $('.buy-product-checkbox-checked').attr('pId');
                var price = parseInt($('.buy-product-checkbox-checked').attr('price'));
                actualPrice = price;
                totalPriceBatchBuy += price;
                selectedToBuy.add(pId);
            }
            if ($('#main-product-has-aggregate-services').length > 0) {
                mainProductHasAggregateServices = $('#main-product-has-aggregate-services').val();
            }
        });

        $(document).ajaxStop(function () {
            bindProductBatchBuyCheckboxes();
            bindBatchBuyButtons();
        });

        function bindProductBatchBuyCheckboxes() {
            $('.buy-product-checkbox').each(function (index) {
                if ($(this).attr('addedClick') != 'yes') {
                    $(this).click(function () {
                        onBatchBuyCheckboxClick(this);
                    });
                    $(this).attr('addedClick', 'yes');
                }
            });
            preCheckProductsSelectedForBuying();
        }

        function bindBatchBuyButtons() {
            $('.btn-batch-buy').unbind('click');

            $('.btn-batch-buy').click(function () {

                onBatchBuyButtonClick(this);
            });
        }

        function onBatchBuyCheckboxClick(sender) {
            var pid = $(sender).attr('rel');
            var price = parseInt($(sender).attr('price'));
            if (sender.checked) {
                if (!selectedToBuy.contains(pid)) {
                    selectedToBuy.add(pid);
                    totalPriceBatchBuy += price;
                }
            } else {
                if (selectedToBuy.contains(pid)) {
                    selectedToBuy.remove(pid);
                    totalPriceBatchBuy -= price;
                }
            }
            preCheckProductsSelectedForBuying();
        }

        function preCheckProductsSelectedForBuying() {
            var pid;
            $('.buy-product-checkbox').each(function (i, chk) {
                pid = $(chk).attr('rel');
                chk.checked = selectedToBuy.contains(pid);
            });
            updateBatchBuySelectionLabels();
        }

        function onBatchBuyButtonClick(sender) {
            var cartUrl = jscheckoutAddUrl;
            var seller = "1";

            if (skuJson_0.skus[0] != null) {
                seller = skuJson_0.skus[0].sellerId;
            }

            if (selectedToBuy.length > 0) {
                jQuery.map(selectedToBuy, function (sku, index) {
                    var skuAdd = "";
                    if (index == 0) {
                        return cartUrl += "?sku=" + sku + "&qty=1&seller=" + seller;
                    } else {
                        return cartUrl += "&sku=" + sku + "&qty=1&seller=" + seller;
                    }

                });

                document.location.href = cartUrl + "&redirect=true&sc=" + jssalesChannel;


            } else {
                alert("Não há produtos selecionados para compra.");
            }
        }

        function updateBatchBuySelectionLabels() {
            $('.selected-count').text(selectedToBuy.length);
            var price = totalPriceBatchBuy / 100;
            $('.selected-value').text(price.toBrazilianCurrency());
        }

        function CheckBatchProductsSelectedForBuying() {
            $('.buy-product-checkbox').each(function (index) {
                if ($(this).is(':checked') && !$(this)[0].hasAttribute("pid")) {
                    selectedToBuy.add($(this).attr('rel'));
                }
            });
        }

        function CheckIfProdcutIsAvailable() {
            if (skuJson.skus.length == 1 && !skuJson.skus[0].available) {
                $('.prateleiracompre.vitrinecompre').hide();
            }
        }
    }
})();

var departamento = (function () {
    var quick_view = {
        open: function () {
            $('.prateleira li .open_quick_view').on('click', function (e) {
                e.preventDefault();

                $('#quick_view, body, #overlay').addClass('active');

                let id = $(this).parents('article').data('id');
                let name = $(this).data('name');
                let por = $(this).data('por');
                let numbers = $(this).data('parc-number');
                let value = $(this).data('parc-value');
                let image = $(this).parents('article').find('.product_image img').attr('src');
                if ($(this).parents('article').find('.flag_discount.active i').length) {
                    let flag = $(this).parents('article').find('.flag_discount.active i').text();
                    $('#quick_view .quick_view_discount').html('<p>' + flag + '%<span>off</span></p>');
                } else {
                    $('#quick_view .quick_view_discount p').remove();
                }

                $('#quick_view .quick_view_name h1').text(name);
                $('#quick_view .quick_view_price .por strong').text(por);
                $('#quick_view .quick_view_price .installments .numbers').text(numbers + 'x');
                $('#quick_view .quick_view_price .installments .value').text(value);
                $('#quick_view .quick_view_image img').attr('src', image);

                vtexjs.catalog.getProductWithVariations(id).done(function (product) {
                    console.log(product);

                    let list_sku = product.skus;

                    $('#quick_view .quick_view_sku ul a').remove();

                    if (product.dimensions[0]) {
                        $(list_sku).each(function (b, al) {
                            $('#quick_view .quick_view_sku').addClass('show');
                            $('#quick_view .quick_view_sku ul').attr('data-length', b);
                            $('#quick_view .quick_view_dimensions').text(product.dimensions[0]);

                            let type_off_dimensions = al.dimensions['' + product.dimensions[0] + ''];

                            if (list_sku.length > 1) {
                                $('#quick_view .quick_view_sku ul').append('<a data-available="' + al.available + '" data-sku="' + al.sku + '" data-dimensions="' + type_off_dimensions + '"><span>' + type_off_dimensions + '</span></a>');
                            } else {
                                $('#quick_view .quick_view_sku ul').append('<a class="checked" data-available="' + al.available + '" data-sku="' + al.sku + '" data-dimensions="' + type_off_dimensions + '"><span>' + type_off_dimensions + '</span></a>');
                                $('#quick_view .quick_view_button').attr('data-sku', al.sku);
                            }

                            $('#quick_view .quick_view_sku a[data-available="true"]').first().trigger('click');
                        });
                    } else {
                        $('#quick_view .quick_view_sku').removeClass('show');
                        $('#quick_view .quick_view_button').attr('data-sku', product.skus[0].sku);
                    }
                });
            });
        },

        choice: function () {
            $(document).on('click', '#quick_view .quick_view_sku a', function (e) {
                e.preventDefault();
                $('#quick_view .quick_view_sku a').removeClass('checked');
                $(this).addClass('checked');

                $('#quick_view .quick_view_button').attr('data-sku', $(this).data('sku'));
            });
        },

        buy: function () {
            $('#quick_view .quick_view_button').on('click', function (e) {
                e.preventDefault();

                $(this).addClass('loading');

                var data_id = $(this).data('sku');

                var item = {
                    id: data_id,
                    quantity: 1,
                    seller: '1'
                };

                vtexjs.checkout.getOrderForm()
                    .done(function (orderForm) {
                        vtexjs.checkout.getOrderForm()
                        vtexjs.checkout.addToCart([item], null, 1).done(function (orderForm) {

                            $('#quick_view .quick_view_button').removeClass('loading');

                            let quantidade = 0;
                            for (var i = orderForm.items.length - 1; i >= 0; i--) {
                                quantidade = parseInt(quantidade) + parseInt(orderForm.items[i].quantity);
                            }

                            $('header .row_1 .cart .qtd').text(quantidade);
                            $('#quick_view .close').trigger('click');
                            open_carrinho();
                        });
                    });
            });
        },

        close: function () {
            $('#quick_view .close').on('click', function (e) {
                e.preventDefault();
                $('#quick_view, body, #overlay').removeClass('active');
            });
        }
    }

    quick_view.open();
    quick_view.choice();
    quick_view.buy();
    quick_view.close();

    var geral = {
        list_sku: function () {
            $('.prateleira ul li article, .cards ul li article').each(function (a, el) {
                vtexjs.catalog.getProductWithVariations($(el).data('id')).done(function (product) {
                    let list_sku = product.skus;

                    $(el).find('.list_sku ol a').remove();

                    if (product.dimensions[0]) {
                        $(list_sku).each(function (b, al) {
                            $(el).find('.list_sku ol').attr('data-length', b);
                            let type_off_dimensions = al.dimensions['' + product.dimensions[0] + ''];

                            if (list_sku.length > 1) {
                                $(el).find('.list_sku ol').append('<a data-available="' + al.available + '" data-sku="' + al.sku + '" data-dimensions="' + type_off_dimensions + '"><span>' + type_off_dimensions + '</span></a>');
                            } else {
                                $(el).find('.list_sku ol').append('<a class="checked" data-available="' + al.available + '" data-sku="' + al.sku + '" data-dimensions="' + type_off_dimensions + '"><span>' + type_off_dimensions + '</span></a>');
                                $(el).find('.btn_buy').attr('data-sku', al.sku);
                            }
                        });
                    } else {
                        $(el).find('.list_sku ol').append('<a class="dis-none"></a>');
                        $(el).find('.btn_buy').attr('data-sku', product.skus[0].sku);
                    }

                    $(el).find('.list_sku ol a[data-available="true"]').first().trigger('click');
                });
            });

            $('.prateleira ul li .list_sku, .cards .list_sku').show();
        },

        choice: function () {
            $(document).on('click', '.list_sku a', function (e) {
                e.preventDefault();
                $(this).siblings('a').removeClass('checked');
                $(this).addClass('checked');

                $(this).parents('article').find('.btn_buy').attr('data-sku', $(this).data('sku'));
            });
        },

        add_to_cart: function () {
            $('.prateleira .btn_buy, .card .btn_buy').on('click', function (e) {
                e.preventDefault();

                if ($(this).data('sku')) {
                    $(this).addClass('loading');

                    $(this).parents('li').addClass('adicionado'); //COMPRE JUNTO

                    var data_id = $(this).data('sku');

                    var item = {
                        id: data_id,
                        quantity: 1,
                        seller: '1'
                    };

                    vtexjs.checkout.getOrderForm()
                        .done(function (orderForm) {
                            vtexjs.checkout.getOrderForm()
                            vtexjs.checkout.addToCart([item], null, 1).done(function (orderForm) {

                                compre_junto();

                                $('.prateleira:not(.compre_junto) .btn_buy.loading, .cards .btn_buy.loading').removeClass('loading');

                                let quantidade = 0;
                                for (var i = orderForm.items.length - 1; i >= 0; i--) {
                                    quantidade = parseInt(quantidade) + parseInt(orderForm.items[i].quantity);
                                }

                                $('header .row_1 .cart .qtd').text(quantidade);

                                open_carrinho();
                            });
                        });
                } else {
                    swal('Oops', 'Selecione uma opção!', 'warning');
                }
            });
        },

        smart_research: function () {
            $('.search-multiple-navigator input[type="checkbox"]').vtexSmartResearch({
                shelfCallback: function () {
                    console.log('shelfCallback');
                    flag_discount();
                },

                ajaxCallback: function () {
                    console.log('ajaxCallback');
                    flag_discount();
                    price_sold_out();
                    geral.list_sku();
                    geral.choice();
                    geral.add_to_cart();
                    quick_view.open();
                    quick_view.choice();
                    quick_view.buy();
                    quick_view.close();
                }
            });
        },

        description: function () {
            $('.departamento .description span').on('click', function () {
                $('.departamento .description').toggleClass('open');
            });
            if ($('.departamento .description p').length == 0) {
                var getCatName = $('.bread-crumb .last a').html()
                $('.departamento .description').append(`<h1 class="onlytitle">${getCatName}</h1>`);
                $('.departamento .description span').remove();
            }
        },

        menuTitle: function () {
            $('.box h5').on('click', function () {
                $(this).toggleClass('actived');
            });
        },

        tags: function () {
            $(document).on('change', '.search-multiple-navigator label input', function () {
                let thisName = $(this).parent().text(),
                    thisClass = $(this).parent().attr('title'),
                    categoriaSelecionada = '<li data-name="' + thisClass + '"><p>' + thisName + '</p><span>x</span></li>';

                if ($(this).parent().hasClass('sr_selected')) {
                    $('.departamento .tags ul').append(categoriaSelecionada);
                } else {
                    $('.departamento .tags ul li[data-name="' + thisClass + '"]').remove();
                }

                $(this).parents('fieldset').find('h5').trigger('click');
            });

            $(document).on('click', '.departamento .tags li', function (event) {
                event.preventDefault();

                $('.search-multiple-navigator label[title="' + $(this).data('name') + '"]').trigger('click');
            });

            $('.departamento .tags #clear').on('click', function (event) {
                event.preventDefault();
                $('.departamento .tags ul li').trigger('click')
            });
        }
    }

    var mobile = {
        open_filter: function () {
            $('.departamento main .box .department_links').prependTo('.search-multiple-navigator');
            $('.departamento main .box .department_links h5').on('click', function () {
                $(this).next('.menu').slideToggle();
            });

            $('.navigation-tabs .search-multiple-navigator fieldset div').prepend('<span class="title"></span>');
            $('.navigation-tabs .menu-departamento>span.rt').text('Filtros');

            $('.resultado-busca-filtro').prepend('<span class="open_filtro">Filtrar</span>');
            $('.departamento .open_filtro').on('click', function () {
                $('.navigation-tabs, #overlay, body').toggleClass('active');
            });

            $('.departamento .navigation-tabs').prepend('<span class="close">X</span>');
            $('.departamento .navigation-tabs .close').on('click', function () {
                $('.departamento .open_filtro').trigger('click');
            });
        },

        label: function () {
            $('.search-multiple-navigator fieldset h5').on('click', function () {
                $(this).parents('fieldset').toggleClass('active');
                $(this).parents('fieldset').find('div').toggleClass('active');
                $('.navigation-tabs .menu-departamento>span.rt').text('Voltar');
                $('.navigation-tabs .menu-departamento>span.rt').addClass('active');
                $(this).parents('fieldset').find('div span.title').text($(this).text());
            });
            $('.navigation-tabs .menu-departamento>span.rt').on('click', function () {
                $('.navigation-tabs .menu-departamento>div .search-multiple-navigator fieldset.active h5').trigger('click');
                $('.navigation-tabs .menu-departamento>span.rt').removeClass('active');
                $(this).text('Filtros');
            });
        }
    }

    if ($('body').hasClass('departamento') || $('body').hasClass('categoria') || $('body').hasClass('busca')) {
        geral.smart_research();
        geral.description();
        geral.tags();
        geral.menuTitle();

        if ($('body').width() < $mobile) {
            mobile.open_filter();
            mobile.label();
        }
    }

    geral.list_sku();
    geral.choice();
    geral.add_to_cart();
})();

var busca = (function () {
    var geral = {
        prateleira: function () {
            $('.buscaVazia .prateleira ul').slick({
                autoplay: false,
                autoplaySpeed: 4000,
                infinite: false,
                arrows: true,
                dots: false,
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }
                ]
            });
        }
    }

    geral.prateleira();
})();

var erros = (function () {
    var geral = {
        empty_search: function () {
            var word = window.location.search.split('?ft=')[1].split('&')[0];
            $(".mesage-content p span").text(decodeURI(word));
        }
    }

    geral.empty_search();
});

if ($('body').hasClass('institucional')) {
    var thisClass = $('body').attr('class');
    var thisClass = thisClass.split(' ')[1];

    $('.sidebar fieldset ul li.' + thisClass + ' a').addClass('ativo');

    var thisClass = $('body').attr('class');
    var thisClass = thisClass.split(' ')[1];

    $('.sidebar fieldset ul li.' + thisClass + ' a').addClass('ativo');

    if ($('body').hasClass('suporte')) {
        $('body.institucional .sidebar').prepend('<h2>Suporte</h2>');
    } else {
        $('body.institucional .sidebar').prepend('<h2>Institucional</h2>');
    }

    $('button.selectFake').live('click', function () {
        $(this).toggleClass('ativo');
        $('.sidebar').slideToggle('fast');
    });

    if ($('body').hasClass('duvidas-frequentes')) {
        $('.perguntas .pergunta label').live('click', function () {
            if ($(this).next('.resposta').is(':visible')) {
                $('.perguntas .pergunta').removeClass('ativo');

                $(this).next('.resposta').slideUp('fast');
            } else {
                $('.perguntas .pergunta').removeClass('ativo');
                $(this).parents('.pergunta').addClass('ativo');

                $('.perguntas .pergunta .resposta').slideUp('fast');
                $(this).next('.resposta').slideToggle();
            }
        });
    }

    if ($('body').hasClass('fale-conosco')) {
        eval(function (p, a, c, k, e, r) {
            e = function (c) {
                return (c < a ? '' : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36))
            };
            if (!''.replace(/^/, String)) {
                while (c--) r[e(c)] = k[c] || e(c);
                k = [function (e) {
                    return r[e]
                }];
                e = function () {
                    return '\\w+'
                };
                c = 1
            };
            while (c--)
                if (k[c]) p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]);
            return p
        }('(5($){$.K.w=5(b,c){2(3.7==0)6;2(14 b==\'15\'){c=(14 c==\'15\')?c:b;6 3.L(5(){2(3.M){3.N();3.M(b,c)}v 2(3.17){4 a=3.17();a.1x(O);a.1y(\'P\',c);a.18(\'P\',b);a.1z()}})}v{2(3[0].M){b=3[0].1A;c=3[0].1B}v 2(Q.R&&Q.R.19){4 d=Q.R.19();b=0-d.1C().18(\'P\',-1D);c=b+d.1E.7}6{t:b,S:c}}};4 q={\'9\':"[0-9]",\'a\':"[A-T-z]",\'*\':"[A-T-1a-9]"};$.1b={1F:5(c,r){q[c]=r}};$.K.U=5(){6 3.1G("U")};$.K.1b=5(m,n){n=$.1H({C:"1I",V:B},n);4 o=D W("^"+$.1J(m.1c(""),5(c,i){6 q[c]||((/[A-T-1a-9]/.1d(c)?"":"\\\\")+c)}).1e(\'\')+"$");6 3.L(5(){4 d=$(3);4 f=D 1f(m.7);4 g=D 1f(m.7);4 h=u;4 j=u;4 l=B;$.L(m.1c(""),5(i,c){g[i]=(q[c]==B);f[i]=g[i]?c:n.C;2(!g[i]&&l==B)l=i});5 X(){x();y();1g(5(){$(d[0]).w(h?m.7:l)},0)};5 Y(e){4 a=$(3).w();4 k=e.Z;j=(k<16||(k>16&&k<10)||(k>10&&k<1h));2((a.t-a.S)!=0&&(!j||k==8||k==1i)){E(a.t,a.S)}2(k==8){11(a.t-->=0){2(!g[a.t]){f[a.t]=n.C;2($.F.1K){s=y();d.G(s.1j(0,a.t)+" "+s.1j(a.t));$(3).w(a.t+1)}v{y();$(3).w(1k.1l(l,a.t))}6 u}}}v 2(k==1i){E(a.t,a.t+1);y();$(3).w(1k.1l(l,a.t));6 u}v 2(k==1L){E(0,m.7);y();$(3).w(l);6 u}};5 12(e){2(j){j=u;6(e.Z==8)?u:B}e=e||1M.1N;4 k=e.1O||e.Z||e.1P;4 a=$(3).w();2(e.1Q||e.1R){6 O}v 2((k>=1h&&k<=1S)||k==10||k>1T){4 p=13(a.t-1);2(p<m.7){2(D W(q[m.H(p)]).1d(1m.1n(k))){f[p]=1m.1n(k);y();4 b=13(p);$(3).w(b);2(n.V&&b==m.7)n.V.1U(d)}}}6 u};5 E(a,b){1o(4 i=a;i<b&&i<m.7;i++){2(!g[i])f[i]=n.C}};5 y(){6 d.G(f.1e(\'\')).G()};5 x(){4 a=d.G();4 b=l;1o(4 i=0;i<m.7;i++){2(!g[i]){f[i]=n.C;11(b++<a.7){4 c=D W(q[m.H(i)]);2(a.H(b-1).1p(c)){f[i]=a.H(b-1);1V}}}}4 s=y();2(!s.1p(o)){d.G("");E(0,m.7);h=u}v h=O};5 13(a){11(++a<m.7){2(!g[a])6 a}6 m.7};d.1W("U",5(){d.I("N",X);d.I("1q",x);d.I("1r",Y);d.I("1s",12);2($.F.1t)3.1u=B;v 2($.F.1v)3.1X(\'1w\',x,u)});d.J("N",X);d.J("1q",x);d.J("1r",Y);d.J("1s",12);2($.F.1t)3.1u=5(){1g(x,0)};v 2($.F.1v)3.1Y(\'1w\',x,u);x()})}})(1Z);', 62, 124, '||if|this|var|function|return|length||||||||||||||||||||||begin|false|else|caret|checkVal|writeBuffer|||null|placeholder|new|clearBuffer|browser|val|charAt|unbind|bind|fn|each|setSelectionRange|focus|true|character|document|selection|end|Za|unmask|completed|RegExp|focusEvent|keydownEvent|keyCode|32|while|keypressEvent|seekNext|typeof|number||createTextRange|moveStart|createRange|z0|mask|split|test|join|Array|setTimeout|41|46|substring|Math|max|String|fromCharCode|for|match|blur|keydown|keypress|msie|onpaste|mozilla|input|collapse|moveEnd|select|selectionStart|selectionEnd|duplicate|100000|text|addPlaceholder|trigger|extend|_|map|opera|27|window|event|charCode|which|ctrlKey|altKey|122|186|call|break|one|removeEventListener|addEventListener|jQuery'.split('|'), 0, {}))

        $('.wrapperContent form input[name="telefone"]').mask("(99)99999-9999");
        $('.wrapperContent form input[name="celular"]').mask("(99)99999-9999");

        $('.wrapperContent form .wrap .item.duplo.assunto select').on('change', function () {
            var text = $('option:selected', this).text();
            $(this).parents('.assunto').find('.selectFake span').html(text);
        });

        $('.wrapperContent form button.enviar').live('click', function () {
            var nome = $('.wrapperContent form input[name="nome"]').val();
            var email = $('.wrapperContent form input[name="email"]').val();
            var telefone = $('.wrapperContent form input[name="telefone"]').val();
            var celular = $('.wrapperContent form input[name="celular"]').val();
            var assunto = $('.wrapperContent form .wrap .item.duplo.assunto button span').html();
            var mensagem = $('.wrapperContent form textarea[name="mensagem"]').val();

            if (nome == "") {
                $('.wrapperContent .mensagem').fadeIn().html("<span style='color: red'>O campo NOME deve ser preenchido!</span>");
                $('.wrapperContent form input[name="nome"]').addClass('error');
                $('.wrapperContent form input[name="nome"]').focus();
                return false;
            }
            if (email == "") {
                $('.wrapperContent .mensagem').fadeIn().html("<span style='color: red'>O campo EMAIL deve ser preenchido!</span>");
                $('.wrapperContent form input').removeClass('error');
                $('.wrapperContent form textarea').removeClass('error');

                $('.wrapperContent form input[name="email"]').addClass('error');
                $('.wrapperContent form input[name="email"]').focus();
                return false;
            }
            var parte1 = email.indexOf("@");
            var parte3 = email.length;
            if (!(parte1 >= 3 && parte3 >= 9)) {
                $('.wrapperContent .mensagem').fadeIn().html("<span style='color: red'>O campo EMAIL deve ser válido!</span>");
                $('.wrapperContent form input').removeClass('error');
                $('.wrapperContent form textarea').removeClass('error');

                $('.wrapperContent form input[name="email"]').addClass('error');
                $('.wrapperContent form input[name="email"]').focus();
                return false;
            }
            if (celular == "") {
                $('.wrapperContent .mensagem').fadeIn().html("<span style='color: red'>O campo CELULAR deve ser preenchido!</span>");
                $('.wrapperContent form input').removeClass('error');
                $('.wrapperContent form textarea').removeClass('error');

                $('.wrapperContent form input[name="celular"]').addClass('error');
                $('.wrapperContent form input[name="celular"]').focus();
                return false;
            }
            if (assunto == "Selecionar") {
                $('.wrapperContent .mensagem').fadeIn().html("<span style='color: red'>O ASSUNTO deve ser selecionado!</span>");
                $('.wrapperContent form input').removeClass('error');
                $('.wrapperContent form textarea').removeClass('error');

                $('.wrapperContent form .wrap .item.duplo.assunto button').addClass('error');
                return false;
            }
            if (mensagem == "") {
                $('.wrapperContent .mensagem').fadeIn().html("<span style='color: red'>Sua MENSAGEM deve ser preenchida!</span>");
                $('.wrapperContent form input').removeClass('error');
                $('.wrapperContent form textarea').removeClass('error');

                $('.wrapperContent form .wrap .item.duplo.assunto button').removeClass('error');
                $('.wrapperContent form textarea[name="mensagem"]').addClass('error');
                $('.wrapperContent form textarea[name="mensagem"]').focus();
                return false;
            }
            $('.wrapperContent .mensagem').fadeIn().html("<span>Aguarde...</span>");

            var dataObj = {
                "nome": nome,
                "email": email,
                "celular": celular,
                "telefone": telefone,
                "assunto": assunto,
                "mensagem": mensagem
            }

            $.ajax({
                url: 'https://api.vtexcrm.com.br/casajnakao/dataentities/FC/documents',
                dataType: 'json',
                type: 'POST',
                crossDomain: true,
                data: JSON.stringify(dataObj),
                headers: {
                    'Accept': 'application/vnd.vtex.ds.v10+json',
                    'Content-Type': 'application/json; charset=utf-8'
                },
                success: function (data) {
                    $('.wrapperContent form input').removeClass('error');
                    $('.wrapperContent form textarea').removeClass('error');
                    $('.wrapperContent .mensagem').fadeIn().html("<span style='color: green'>Enviado com sucesso!</span>");

                    $('.wrapperContent form .wrap .item.duplo.assunto button span').html('Selecionar');
                    $('.wrapperContent form input').val("");
                    $('.wrapperContent form textarea').val("");
                }
            });
            return false;
        })
    }
}