const { src, dest, watch, series, parallel } = require("gulp");
var sass = require("gulp-sass")(require("sass"));
const autoprefixer = require("gulp-autoprefixer");
const csso = require("gulp-csso");
const babel = require("gulp-babel");
const terser = require("gulp-terser");
const rename = require("gulp-rename");
const uglify = require("gulp-uglify");
const webpack = require("webpack-stream");
const os = require("os");
const dateFormat = require("dateformat");
const tap = require("gulp-tap");
const sourcemaps = require("gulp-sourcemaps");
const cleanCSS = require("gulp-clean-css");
const mode = require("gulp-mode")();
const path = require("path");

var commentCss = "/*\n User: " + os.hostname() + " \n Date " + dateFormat(new Date(), "dd/mm/yyyy HH:MM:ss Z") + "\n*/\n";

// css task
const css = () => {
  return src("assets/css/sass/style.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(
      tap(function (file) {
        file.contents = Buffer.concat([Buffer.from(commentCss), file.contents]);
      })
    )
    .pipe(rename("all.min.css"))
    .pipe(dest("./files/"));
};

const cssProd = () => {
  return src("assets/css/sass/style.scss")
    .pipe(mode.development(sourcemaps.init()))
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer())
    .pipe(mode.production(csso()))
    .pipe(mode.development(sourcemaps.write()))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(
      tap(function (file) {
        file.contents = Buffer.concat([Buffer.from(commentCss), file.contents]);
      })
    )
    .pipe(rename("all.min.css"))
    .pipe(dest("./files/"));
};

// js task
const js = () => {
  return src("assets/js/all.js")
    .pipe(
      babel({
        presets: ["@babel/env"],
      })
    )
    .pipe(
      webpack({
        mode: "development",
        devtool: "inline-source-map",
      })
    )
    .pipe(mode.development(sourcemaps.init({ loadMaps: true })))
    .pipe(rename("all.min.js"))
    .pipe(mode.production(terser({ output: { comments: false } })))
    .pipe(mode.development(sourcemaps.write()))
    .pipe(uglify({ compress: false, mangle: false }))
    .pipe(dest("./files/"));
};

// watch task
const watchForChanges = () => {
  watch("assets/css/sass/*.scss", css);
  watch("assets/js/**/*.js", js);
};

// public tasks
exports.default = series(parallel(css, js), watchForChanges);
exports.build = series(parallel(cssProd, js));
